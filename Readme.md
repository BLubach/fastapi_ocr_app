# Fastapi OCR server

## Project description
This is a demo template project mainly used as reference/template for my other projects on how to convert and implement OCR (or other ai model types) into a web microservice environment. 
Adapatability of the fastapi app allows for easy modification of the ocr response (see circle drawing in the later examples), which might be more suited to custom application than standard OCR services. 


### Stack
- Fastapi
- OCR (Pytorch/easyOCR)
- (no database)


## Template for

### Authentication
JWT webtoken verification is supported, no signing of JWT keys as no user model is included. 

### OCR 
The main concept of this template is to setup a simple Fastapi webserver as part of a microservice. The included docker container can be used as part of a larger docker-compose file or as a separate server complete to allow for better scalability. 
It deliberately keps as a small project so it can be setup on a GPU Parperspace/digitalocean droplet or Amazon EC2 P2 Instance. 

## Functionality
### Input
Image with text to be scanned

#### Supported image types
- PNG
- JPEG
- GIF
- BMP
- Webp


### Configuration (handled by different endpoints)
- OCR settings
- Object detection (only circles supported here)


### Output
- Image with detected text drawn (fastapi streamingresponse) 
- Image with objects/text drawn (fastapi streamingresponse)
- Textdata and coordinates

Data output format

```
[
    {
        "bounding_box": {
            "x_min": 38.0,
            "y_min": 15.0,
            "x_max": 171.0,
            "y_max": 77.0
        },
        "text": "NLON"
    },
    {
        "bounding_box": {
            "x_min": 27.0,
            "y_min": 116.0,
            "x_max": 97.0,
            "y_max": 152.0
        },
        "text": "Kopie"
    },
  ....
]
```


## Examples

### Invoice table text 
![Invoice](examples/energierekening.png){: width="45%"} ![Invoice scanned](examples/energierekening_scanned.png){: width="45%"}

### Car dashboard
![Invoice](examples/car_dashboard.jpeg){: width="45%"} ![Invoice scanned](examples/car_dashboard_scanned.png){: width="45%"}

### P&ID 
Showcases the possibility to include specific objects that can be scanned. 

![Invoice](examples/P&ID.png){: width="45%"} ![Invoice scanned](examples/P&ID_circles.png){: width="45%"}

## Todo:
- Support all image types and pdf
- Celery/redis job scheduling
- More generic object detection
- GPU support (on server with dedicated GPU)

