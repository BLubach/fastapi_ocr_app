from typing import List
from pydantic import BaseModel

class ImageInput(BaseModel):
    image_data: str  

class ImageOutput(BaseModel):
    processed_image: str  


class BoundingBox(BaseModel):
    x_min: float
    y_min: float
    x_max: float
    y_max: float

class DetectedObject(BaseModel):
    bounding_box: BoundingBox
    text: str

class DetectedObjectsResponse(BaseModel):
    text_data: List[DetectedObject]