from fastapi import File, UploadFile, Depends
from fastapi import APIRouter
from app.authentication.verify_token import verify_token
from app.api.service.ocrservice import (
    detect_and_return_data,
    detect_and_draw_text,
    detect_and_draw_circles,
    extract_text_within_circles_return_data
)


ocr_router = APIRouter()


@ocr_router.post("/scan-image-text/")
async def scan_image_endpoint(file: UploadFile = File(...)):

    # Read the uploaded file into memory
    contents = await file.read()

    # Process the image and extract text and bounding boxes
    streaming_reponse = detect_and_draw_text(contents)

    return streaming_reponse


@ocr_router.post("/scan-image-return-data/")
async def scan_image_endpoint_return_data(file: UploadFile = File(...)):

    # Read the uploaded file into memory
    contents = await file.read()

    # Process the image and extract text and bounding boxes
    return_data = detect_and_return_data(contents)

    return return_data


@ocr_router.post("/scan-image-circles/")
async def scan_image_endpoint_circles(file: UploadFile = File(...)):

    # Read the uploaded file into memory
    contents = await file.read()

    # Process the image, draw circles and return the image as a streaming response
    streaming_reponse = detect_and_draw_circles(contents)

    return streaming_reponse


@ocr_router.post("/scan-image-circles-return-data/")
async def scan_image_endpoint_circles_return_data(file: UploadFile = File(...)):

    # Read the uploaded file into memory
    contents = await file.read()

    # Process the image and extract text within circles and return the data
    return_data = extract_text_within_circles_return_data(contents)

    return return_data




@ocr_router.get("/test/")
async def test_endpoint():
    return {"message": "Hello from fastapi!"}


# @ocr_router.post("/protected-scan-image/")
# async def protected_scan_image_endpoint(file: UploadFile = File(...), token_data=Depends(verify_token)):

#     # Read the uploaded file into memory
#     contents = await file.read()

#     processed_image_bytes = extract_text_from_image(contents)

#     # Return the processed image as a streaming response
#     return StreamingResponse(BytesIO(processed_image_bytes), media_type="image/png")
