from app.api.utils.image_operations import draw_bounding_boxes, grayscale_open_cv_image, draw_circles
from app.api.utils.file_conversion import pil_to_bytes, convert_pil_to_opencv, image_to_PIL
from app.api.utils.ocr import detect_text, detect_circles, detect_text_within_circles
from app.api.dto.ImageDTO import BoundingBox, DetectedObject
from fastapi.responses import StreamingResponse
from io import BytesIO
import cv2

def detect_and_return_data(image): 
    """
    Takes image as argument, returns text extracted from image using BoundBox DTO (pydantic model)
    Format: 
    
    """
    # 1. convert image to PIL
    pil_image = image_to_PIL(image)

    # 2. convert PIL image to OpenCV format
    open_cv_image = convert_pil_to_opencv(pil_image)    

    # 3. Process the image using easyocr
    OCR_results = detect_text(open_cv_image)


    # 4. Create a list of DetectedObject instances
    detected_objects = []
    for (bbox, text, _) in OCR_results:
        bounding_box = BoundingBox(x_min=bbox[0][0], y_min=bbox[0][1], x_max=bbox[2][0], y_max=bbox[2][1])
        detected_object = DetectedObject(bounding_box=bounding_box, text=text)
        detected_objects.append(detected_object)

    return detected_objects




def detect_and_draw_text(image):
    """
    Takes image as argument, returns streamingresponse image with bounding boxes around detected text
    """

    # 1. convert image to PIL
    pil_image = image_to_PIL(image)

    # 2. convert PIL image to OpenCV format
    open_cv_image = convert_pil_to_opencv(pil_image)    

    # 3. Process the image using easyocr
    OCR_results = detect_text(open_cv_image)

    # 4. Draw bounding boxes around detected text
    drawn_image = draw_bounding_boxes(pil_image, OCR_results)

    # 5. Convert the processed image back to bytes
    processed_image_bytes = pil_to_bytes(drawn_image)

    return StreamingResponse(BytesIO(processed_image_bytes), media_type="image/png")





def detect_and_draw_circles(image):
    """
    Detect circles in an image, returns image with circles drawn on it
    """ 

    # 1. convert image to PIL
    pil_image = image_to_PIL(image)

    # 2. convert PIL image to OpenCV format
    open_cv_image = convert_pil_to_opencv(pil_image)    

    # 3. Convert the image to grayscale
    # grayscaled_image = grayscale_open_cv_image(open_cv_image)
    grayscaled_image = cv2.cvtColor(open_cv_image, cv2.COLOR_BGR2GRAY)

    # 4. Detect circles in the image
    circles = detect_circles(grayscaled_image)

    # # 5. Draw circles on the image    
    drawn_image = draw_circles(pil_image, circles)

    # # 6. Convert the processed image back to bytes
    processed_image_bytes = pil_to_bytes(drawn_image)


    return StreamingResponse(BytesIO(processed_image_bytes), media_type="image/png")




def extract_text_within_circles_return_data(image):
    """
    Takes image as argument, returns text within circles in image
    Format:
    """

    # 1. convert image to PIL
    pil_image = image_to_PIL(image)

    # 2. convert PIL image to OpenCV format
    open_cv_image = convert_pil_to_opencv(pil_image)
    
    # 3. Convert the image to grayscale
    grayscaled_image = grayscale_open_cv_image(open_cv_image)

    # 4. Detect circles in the image
    circles = detect_circles(grayscaled_image)
    

    # 5. Extract text within circles
    OCR_results = detect_text_within_circles(open_cv_image, circles)

    # 6. Create a list of DetectedObject instances
    detected_objects = []
    for (bbox, text, _) in OCR_results:
        bounding_box = BoundingBox(x_min=bbox[0][0], y_min=bbox[0][1], x_max=bbox[2][0], y_max=bbox[2][1])
        detected_object = DetectedObject(bounding_box=bounding_box, text=text)
        detected_objects.append(detected_object)

    return detected_objects
    
