from io import BytesIO
from PIL import Image
import cv2
import numpy as np


def image_to_PIL(in_memory_image):
    """
    Convert image to PIL format
    """


    pil_image = Image.open(BytesIO(in_memory_image))
   
    return pil_image



def pil_to_bytes(pil_image):
    """
    Convert PIL Image to bytes
    """

    img_byte_array = BytesIO()
    pil_image.save(img_byte_array, format="PNG")
    img_byte_array = img_byte_array.getvalue()
    return img_byte_array



def convert_pil_to_opencv(pil_image):
    """
    Convert image to OpenCV format
    """
    
    open_cv_image = cv2.cvtColor(np.array(pil_image), cv2.COLOR_RGB2BGR)

    return open_cv_image

def convert_opencv_to_PIL(open_cv_image):
    """
    Convert image to OpenCV format
    """
    
    pil_image = Image.fromarray(open_cv_image)

    return pil_image