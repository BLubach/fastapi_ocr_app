from PIL import ImageDraw, ImageFont
import numpy as np
import cv2

def rotate_image(pil_image):
    """
    Rotate image by 90 degrees, requires PIL Image as input
    """
    # Rotate the image by 90 degrees
    pil_image = pil_image.rotate(90)
    
    return pil_image


def grayscale_open_cv_image(open_cv_image):
    """
    Convert OpenCV image to grayscale
    """

    grayscaled_image = cv2.cvtColor(open_cv_image, cv2.COLOR_BGR2GRAY)

    return grayscaled_image



def draw_bounding_boxes(image, ocr_result_data):
    """
    Draw bounding boxes around detected text
    Requires PIL Image as input and OCR results list
    OCR results list should be in the following format:
    [
        (
            [ # Bounding box coordinates
                (x1, y1),
                (x2, y2),
                (x3, y3),
                (x4, y4)
            ],
            "Detected text",
            0.9 # Confidence level
        ),
        ...
    ]
    """

    # Create a drawing context on the image
    draw = ImageDraw.Draw(image)

    # Define font and size for drawing text (you can adjust these values)
    font = ImageFont.load_default()
    font_size = 20

    # Iterate through the OCR results list
    for ocr_result_data in ocr_result_data:
        bounding_box, detected_text, _ = ocr_result_data

        # Extract coordinates from the bounding box
        x1, y1 = bounding_box[0]
        x2, y2 = bounding_box[2]

        # Draw the bounding box around the text
        draw.rectangle([x1, y1, x2, y2], outline="red", width=2)
        draw.text((x1, y1 - font_size), detected_text, fill="red", font=font)

    # return image with bounding boxes and detected text
    return image




def draw_circles(image, circleData): 

    if circleData is None:
        return image
    
    
    # Create a drawing context on the image
    draw = ImageDraw.Draw(image)

    # Convert the circle parameters to integers
    circles = np.uint16(np.around(circleData))

    # Draw circles on the image
    for i in circles[0, :]:
        bbox = (i[0] - i[2], i[1] - i[2], i[0] + i[2], i[1] + i[2])
        draw.ellipse(bbox, outline=(0, 255, 0), width=2)


    return image