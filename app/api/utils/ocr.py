

import easyocr
import cv2
import numpy as np
from app.api.utils import ocr_settings


def detect_text(open_cv_image):
    """
    Runs easyocr on an image (in open cv2 image format), using the provided settings
    Returns results directly
    """
    
    reader = easyocr.Reader(
        **ocr_settings.basic_reader_settings
    )

    results = reader.readtext(
        open_cv_image,
        **ocr_settings.default_readtext_settings
    )

    return results


def detect_circles(grayscaled_open_cv_image):
    """
    Detect circles in an image, returns circledata
    """

    # Apply Gaussian blur
    blurred = cv2.GaussianBlur(grayscaled_open_cv_image, (17,17), 0)

    # Define parameters for circle detection
    minDist = 1
    param1 = 45
    param2 =  100
    minRadius = 40
    maxRadius = 100

    # Detect circles using Hough transform
    circles = cv2.HoughCircles(
        blurred, cv2.HOUGH_GRADIENT, 1, minDist, param1=param1, param2=param2,
        minRadius=minRadius, maxRadius=maxRadius
    )

    return circles



def detect_text_within_circles(grayscaled_open_cv_image, circles):
    """
    Runs easyocr on an grayscale_open_cv_image, limits textdetection to a space round a detected object (circle in this case) using the provided settings
    Returns results directly
    """
    if circles is None:
        return []


    reader = easyocr.Reader(
        **ocr_settings.basic_reader_settings
    )

    # Convert the circle parameters to integers
    circles = np.uint16(np.around(circles))

    
    total_results = []

    # Iterate through the circle parameters
    for i in circles[0,:]:
        center_x, center_y, radius = i[0], i[1], i[2]

        # Crop the region of interest within the circle
        sections_to_scan = grayscaled_open_cv_image[center_y - radius:center_y + radius, center_x - radius:center_x + radius]

        # Perform text detection within the circle region
        results = reader.readtext(
            sections_to_scan,
            **ocr_settings.default_readtext_settings
        )



        # Append the results to the return data
        total_results.extend(results)


    return total_results

