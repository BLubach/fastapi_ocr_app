
basic_reader_settings = {
    'lang_list': ['en'],
    'gpu':  False,
}


default_reader_settings = {
    'lang_list': ['en'],
    'gpu':  False,
    'model_storage_directory':  None,
    'download_enabled':  True,
    'user_network_directory':  None,
    'recog_network': 'standard',
    'detector': 'craft',
    'recognizer': 'transformer',
}

default_readtext_settings = {
    'decoder': 'greedy',        # greedy, beamsearch, wordbeamsearch
    # 'beam_width': 5,          # only for beamsearch and wordbeamsearch
    'batch_size':  1,
    'workers':  0,              # Force EasyOCR to recognize only subset of characters. Useful for specific problem (E.g. license plate, etc.)
    'allowlist': None,          # Block subset of characters. Useful for specific problem (E.g. license plate, etc.)
    'blocklist': None,          # 0 = box, 1 = box + line, 2 = box + line + word, 3 = box + line + word + score
    'detail': 0,
    'paragraph': False,         # Combine result into paragraph
    'min_size': 20,             # Minimum detection text size
    'rotation_info': None,      # Additional image rotation info. 0 = 0 degrees, 1 = 90 degrees, 2 = 180 degrees, 3 = 270 degrees
    'contrast_ths': 0.1,        # Text box contrast filter threshold
    'adjust_contrast': 0.5,     # Text box contrast value
    'text_threshold': 0.7,      # Text confidence threshold
    'low_text': 0.4,            # Text low-bound score
    'link_threshold': 0.4,      # Link confidence threshold
    'canvas_size': 2560,        # Maximum image size. Image bigger than this value will be resized down.
    'mag_ratio': 1.5,           # Image magnification ratio
    'slope_ths': 0.1,           # Slope threshold to merge boxes
    'ycenter_ths': 0.5,         # Maximum line deviation from the horizontal center
    'height_ths': 0.5,          # Maximum text height deviation from the median height
    'width_ths': 0.5,           # Maximum text width deviation from the median width
    'add_margin': 0.1,          # Extend bounding boxes in all direction by certain value. This is important for language with complex script (E.g. Thai).
    'x_ths': 0.1,               # Maximum horizontal gap between letters when merging text boxes.
    'y_ths': 0.1,               # Maximum vertical gap between letters when merging text boxes.  
}


milage_readtext_settings = {
    'decoder': 'greedy',        # greedy, beamsearch, wordbeamsearch
    'workers':  0,              # Force EasyOCR to recognize only subset of characters. Useful for specific problem (E.g. license plate, etc.)
    'allowlist': '1234567890',  # Block subset of characters. Useful for specific problem (E.g. license plate, etc.)
}


default_readtext_settings = {
    'decoder': 'greedy',        # greedy, beamsearch, wordbeamsearch
    'workers':  0,              # Force EasyOCR to recognize only subset of characters. Useful for specific problem (E.g. license plate, etc.)
    'allowlist': 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890',  # Block subset of characters. Useful for specific problem (E.g. license plate, etc.)
}
