from fastapi import HTTPException, Depends, Header
import jwt
from typing import Optional
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
import os


public_key = "simpleinsercurekey"


def verify_token(authorization: Optional[str] = Header(None)):
   
    if authorization is None:
        raise HTTPException(status_code=401, detail="Missing token")
    
  
    # remove "Bearer " from token
    token = authorization.split(" ")[1]


    try:
        payload = jwt.decode(token, public_key, algorithms=["HS256"])
        # Here, you might perform additional validation or checks on the payload if needed
        
        return payload  # Return the payload if the token is valid

    except (jwt.PyJWTError, FileNotFoundError, ValueError):
        raise HTTPException(status_code=401, detail="error")
