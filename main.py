from fastapi import FastAPI
from app.api.endpoints.ocr import ocr_router 

app = FastAPI()

app.include_router(ocr_router)